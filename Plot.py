import matplotlib.pyplot as plt
import numpy as np

x = np.arange(0, 10, 2)
y1 = x
y2 = x ** 2

plt.plot(x, y1, '*g--')
plt.show()

# 正常显示中文
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']

plt.xlabel('自变量')   # 若是使用 汉字，则显示出错
plt.ylabel('因变量')
plt.plot(x, y1, '*g--', y2, '^b-')
plt.legend(['y=x', '$y=x^2$'], loc='upper right')  # 显示每条线段的解释， $$ 里是 LaTeX语句
plt.show()

# 1.创建画板fig
fig = plt.figure()
# 参数解释,前两个参数 1,2 表示创建了一个一行两列的框 第三个参数表示当前所在的框
ax1 = fig.add_subplot(1, 2, 1)
ax2 = fig.add_subplot(1, 2, 2)
ax1.plot(x, y1, '*--', color="tab:blue")
ax2.plot(x, y2, '^-', color='tab:orange')
plt.show()

fig1 = plt.figure(num=1, figsize=(7, 5))
x = np.linspace(0.0, np.pi * 2, 20)
y = np.sin(x)

plt.plot(x, y, 'rx-', x, 2 * x, 'go-.')  # 每条都指定x轴数据

fig2 = plt.figure(num=2)
plt.plot(x, y, 'rx-', 2 * x, 'go-.')  # 一条指定x轴数据，其他不指定

fig2 = plt.figure(num=3)
plt.plot(y, 'rx-', 2 * x, 'go-.')  # 都不指定
plt.show()
