import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2

# Load image
img = cv2.imread('images/006.jpg')

# Apply thresholding
"""
这个函数的第一个参数就是原图像，原图像应该是灰度图。
第二个参数就是用来对像素值进行分类的阈值。
第三个参数就是当像素值高于（有时是小于）阈值时应该被赋予的新的像素值
第四个参数来决定阈值方法，见threshold_simple()
"""
binary = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY)[1]

save_file = './ocr_result/binary_image.jpg'
# Save output image
cv2.imwrite(save_file, binary)

# 显示图片--二分值
img1 = mpimg.imread(save_file)
plt.figure(figsize=(10, 10))
plt.imshow(img1)
plt.axis('off')
plt.show()
