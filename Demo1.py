import numpy as np

# 标量 = 0维  ，单行用#注释
x = np.array(12)
print(x)
print('几维 => ', x.ndim)
print('\n')

print("np.arange(7): ", np.arange(7))  # 终点值为7，默认起点值为0，步长为1；
print("np.arange(3,7): ", np.arange(3, 7))  # 终点值为7，起点值为3，默认步长为1；
print("np.arange(3,7,2): ", np.arange(3, 7, 2))  # 终点值为7，起点值为3，步长为2；

"""
多行用 这个注意
创建 一维数组 ，21 个值，
"""
arr = np.arange(21)
print(arr)
print('几维 => ', arr.ndim)
print('\n')

"""
创建二维数组
"""
arr = np.array([[0, 1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12, 13], [14, 15, 16, 17, 18, 19, 20]])
print(arr)
print('几维 => ', arr.ndim)
print('\n')

'''
创建21一个数字的数组，
将它转成 二维数组 3 个 7 
'''
arr = np.arange(21)
arr.shape = (3, 7)  # 转成二维数组
print(arr)
print('几维 => ', arr.ndim)
print('\n')

'''
三维数组
'''
arr = np.arange(27)
arr.shape = (3, 3, 3)  # 魔方
print(arr)
print('几维 => ', arr.ndim)
print('\n')

arr = np.ones(54)
arr.shape = (2, 3, 3, 3)  # 2 个 三维数组  => 4维
print(arr)
print('几维 => ', arr.ndim)
print('\n')
