import paddle
import numpy as np

A = paddle.reshape(paddle.arange(40), (2, 5, 4))
print(A)

A_T = paddle.transpose(A, perm=[2, 0, 1])  # shape=[2, 5, 4] => shape=[4, 2, 5]
print(A_T)

B = paddle.to_tensor([[1, 2, 3], [2, 0, 4], [3, 4, 5]])
B_T = paddle.transpose(B, perm=[1, 0])
print(B)
print(B_T)
print(B == B_T)

print(B * B_T)

# 降维
print(A.sum())

arr = paddle.arange(15)
D = paddle.reshape(arr, (5, 3))
print(D)

# D_S = paddle.sum(D, axis=[1, 2], keepdim=True)  # keepdim=True 保持维度，用于广播
D_S = paddle.sum(D, axis=[1])  # 相当于把1维度消掉，留下5
print(D_S)
# print(D / D_S)
#
# # 范数
# u = paddle.to_tensor([1.0, 2.0])
# print(paddle.norm(u))


arr = paddle.arange(24)
D = paddle.reshape(arr, (2, 3, 4))
print(D)
D_S = paddle.sum(D, axis=[1])  # 相当于把1维度消掉，留下,D_S.shape => [2,4]  , 对哪一维度求和，就消除哪一维度
print(D_S)