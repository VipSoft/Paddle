import os
import matplotlib.pyplot as plt
from paddleocr.ppocr.data.imaug.label_ops  import DetLabelEncode
from paddleocr.ppocr.data.imaug.make_border_map  import MakeBorderMap

label_path = "../train_data/icdar2015/text_localization/train_icdar2015_label.txt"
img_dir = "../train_data/icdar2015/text_localization/"

# 1. 读取训练标签的第一条数据
f = open(label_path, "r")
lines = f.readlines()

# 2. 取第一条数据
line = lines[0]

print("The first data in train_icdar2015_label.txt is as follows.\n", line)
img_name, gt_label = line.strip().split("\t")

# 3. 读取图像
image = open(os.path.join(img_dir, img_name), 'rb').read()
data = {'image': image, 'label': gt_label}

# 1. 声明标签解码的类
decode_label = DetLabelEncode()
# 2. 打印解码前的标签
print("The label before decode are: ", data['label'])
data = decode_label(data)
print("\n")

# 4. 打印解码后的标签
print("The polygon after decode are: ", data['polys'])
print("The text after decode are: ", data['texts'])


# 1. 声明MakeBorderMap函数
generate_text_border = MakeBorderMap()

# 2. 根据解码后的输入数据计算bordermap信息
data = generate_text_border(data)

# 3. 阈值图可视化
src_img = data['image']
plt.figure(figsize=(10, 10))
plt.imshow(src_img)

text_border_map = data['threshold_map']
plt.figure(figsize=(10, 10))
plt.imshow(text_border_map)
plt.show()
