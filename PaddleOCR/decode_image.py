import os
import matplotlib.pyplot as plt
from paddleocr.ppocr.data.imaug.operators import DecodeImage


label_path = "../train_data/icdar2015/text_localization/train_icdar2015_label.txt"
img_dir = "../train_data/icdar2015/text_localization/"

# 1. 读取训练标签的第一条数据
f = open(label_path, "r")
lines = f.readlines()

# 2. 取第一条数据
line = lines[0]

print("The first data in train_icdar2015_label.txt is as follows.\n", line)
img_name, gt_label = line.strip().split("\t")

# 3. 读取图像
image = open(os.path.join(img_dir, img_name), 'rb').read()
data = {'image': image, 'label': gt_label}

# 4. 声明DecodeImage类，解码图像
decode_image = DecodeImage(img_mode='RGB', channel_first=False)
data = decode_image(data)

# 5. 打印解码后图像的shape，并可视化图像
print("The shape of decoded image is ", data['image'].shape)

plt.figure(figsize=(10, 10))
plt.imshow(data['image'])
src_img = data['image']
plt.show()


