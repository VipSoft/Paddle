import cv2
import os
import matplotlib.pyplot as plt
from paddleocr import PaddleOCR, draw_ocr

# PaddleOCR目前支持中英文、英文、法语、德语、韩语、日语，可以通过修改lang参数进行切换
# 参数依次为`ch`, `en`, `french`, `german`, `korean`, `japan`。
ocr = PaddleOCR(use_angle_cls=True, lang="ch", use_gpu=False)  # need to run only once to download and load model into memory
save_results = []
img_path = 'images/008.jpg'
save_dir = 'ocr_result'
result = ocr.ocr(img_path, cls=True)[0]
# 将结果写入文件
with open(
        os.path.join(save_dir, "008_result.txt"),
        'w',
        encoding='utf-8') as f:
    for line in result:
        f.writelines(str(line)+'\n')
        print(line)

# 显示结果
from PIL import Image

image = Image.open(img_path).convert('RGB')
boxes = [line[0] for line in result]
txts = [line[1][0] for line in result]
scores = [line[1][1] for line in result]
im_show = draw_ocr(image, boxes, txts, scores, font_path='./fonts/simfang.ttf')
cv2.imwrite(os.path.join(save_dir, "008_result.jpg"), im_show)
plt.figure(figsize=(15, 8))
plt.imshow(im_show)
plt.show()
