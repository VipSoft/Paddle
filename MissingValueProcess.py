import os
import pandas as pd

# 添加 测试数据
os.makedirs(os.path.join('.', 'data'), exist_ok=True)
data_file = os.path.join('.', 'data', 'house_tiny.csv')
# with open(data_file, 'w') as f:
#     f.write('NumRooms,Alley,Test,Price\n')
#     f.write('NA,Pave,NA,127500\n')
#     f.write('2,D,A,106000\n')
#     f.write('4,NA,NA,178100\n')
#     f.write('NA,NA,B,14000\n')

# 读取 csv 数据
data = pd.read_csv(data_file)
print("\nCSV data => \n", data)
print("-" * 60)

# 检测缺失值
res_null = pd.isnull(data)
print("\nres_null => \n", res_null)
print("\nres_null.sum() => \n", res_null.sum())

# 通过位置索引iloc，将 data 分成 inputs、 outputs
inputs, outputs = data.iloc[:, 0:3], data.iloc[:, 3]

print("-" * 60)

# 处理缺失值

"""
删除法：
简单，但是容易造成数据的大量丢失
how = "any"  只要有缺失值就删除
how = "all"  只有全行或者全列都为缺失值才删除
"""
print("-" * 60)
data.dropna(how="all", axis=0, inplace=True)
print("删除之后的结果，全行或全列全缺失删除: \n", data)
data.dropna(how="any", axis=0, inplace=True)
print("删除之后的结果: \n", data)
data.dropna(how="any", axis=1, inplace=True)
print("删除之后的结果: \n", data)

"""
填充法:
只要不影响数据分布或者对结果影响不是很大的情况
数值型 ——可以使用均值、众数、中位数来填充，也可以使用这一列的上下邻居数据来填充
类别数据(非数值型) ——可以使用众数来填充，也可以使用这一列的上下邻居数据来填充
使用众数来填充非数值型数据
"""
print("-" * 60)
# 处理缺失值，替换法 - 用当前列的平均值，填充 NaN
inputs = inputs.fillna(inputs.mean())
print("\ninputs.fillna => \n", inputs)



import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import lagrange
# 插值法
# 线性插值 ——你和线性关系进行插值
# 多项式插值 ——拟合多项式进行插值
# 拉格朗日多项式插值、牛顿多项式插值
# 样条插值 ——拟合曲线进行插值
x = np.array([1, 2, 3, 4, 5, 8, 9])
y = np.array([3, 5, 7, 9, 11, 17, 19])
z = np.array([2, 8, 18, 32, 50 ,128, 162])

# 线型插值
linear_1 = interp1d(x=x, y=y, kind="linear")
linear_2 = interp1d(x=x, y=z, kind="linear")
linear_3 = interp1d(x=x, y=y, kind="cubic")


print("线性插值: \n", linear_1([6, 7])) # [13. 15.]  注意不是1是第一个索引
# print("线性插值: \n", linear_1([5, 6])) # [11. 13.]
print("线性插值: \n", linear_2([6, 7])) # [76. 102]
print("线性插值: \n", linear_3([6, 7])) # [76. 102]

# 拉格朗日插值
la_1 = lagrange(x=x, w=y)
la_2 = lagrange(x=x, w=y)

print("拉格朗日: \n",  la_1)  # [13, 15]
print("拉格朗日: \n",  la_2)  # [72, 98]

# 样条插值 ——拟合曲线进行差池

# 对于线型关系，线型插值，表现良好，多项式插值，与样条插值也表现良好
# 对于非线型关系，线型插值，表现不好，多项式插值，与样条插值表现良好
# 推荐如果想要使用插值方式，使用拉格朗日插值和样条插值


# 对于非NaN类型的数据——先将非NaN类型的数据转化为np.nan
data.replace("*", np.nan, inplace=True)
print("data: \n", data)
print(type(np.nan))


print("-" * 60)
# 把离散的类别信息转化为 one-hot 编码形式
inputs = pd.get_dummies(inputs, dummy_na=True)
print("\none-hot => \n", inputs)

import paddle

# 转换为张量格式
x, y = paddle.to_tensor(inputs.values), paddle.to_tensor(outputs.values)
print("\n to_tensor => \n", x, y)
